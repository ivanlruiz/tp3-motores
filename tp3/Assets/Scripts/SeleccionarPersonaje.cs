using System.Collections;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    public virtual void Atacar() { }
}

public class Mago : Personaje
{
    public GameObject[] proyectiles;
    public float fuerzaLanzamiento = 10f;

    Animator animator;
    MovJugador movimiento;

    void Start()
    {
        animator = GetComponent<Animator>();
        movimiento = GetComponent<MovJugador>();
    }

    public override void Atacar()
    {
        LanzarProyectiles();
    }

    void LanzarProyectiles()
    {
        // Obtener la direcci�n hacia donde apunta el mouse
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            Vector3 direccion = (hit.point - transform.position).normalized;

            // Lanzar cada proyectil en la direcci�n calculada
            foreach (GameObject proyectil in proyectiles)
            {
                GameObject nuevoProyectil = Instantiate(proyectil, transform.position + direccion, Quaternion.identity);
                Rigidbody rbProyectil = nuevoProyectil.GetComponent<Rigidbody>();
                rbProyectil.AddForce(direccion * fuerzaLanzamiento, ForceMode.Impulse);
            }
        }
    }
}

public class Ninja : Personaje
{
    public GameObject[] proyectiles;
    public float fuerzaLanzamiento = 10f;

    Animator animator;
    MovJugador movimiento;

    void Start()
    {
        animator = GetComponent<Animator>();
        movimiento = GetComponent<MovJugador>();
    }

    public override void Atacar()
    {
        LanzarProyectiles();
    }

    void LanzarProyectiles()
    {
        // Obtener la direcci�n hacia donde apunta el mouse
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            Vector3 direccion = (hit.point - transform.position).normalized;

            // Lanzar cada proyectil en la direcci�n calculada
            foreach (GameObject proyectil in proyectiles)
            {
                GameObject nuevoProyectil = Instantiate(proyectil, transform.position + direccion, Quaternion.identity);
                Rigidbody rbProyectil = nuevoProyectil.GetComponent<Rigidbody>();
                rbProyectil.AddForce(direccion * fuerzaLanzamiento, ForceMode.Impulse);
            }
        }
    }
}

public class SeleccionarPersonaje : MonoBehaviour
{
    public GameObject personajeSeleccionado;
    public Personaje scriptPersonajeActual;
    private bool ataqueEnProceso = false;

    void Update()
    {
        // Seleccionar personaje con clic izquierdo
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                GameObject objetoTocado = hit.transform.gameObject;
                if (objetoTocado.CompareTag("Player"))
                {
                    // Desmarcar personaje previamente seleccionado
                    if (personajeSeleccionado != null)
                    {
                        Renderer renderer = personajeSeleccionado.GetComponent<Renderer>();
                        renderer.material.color = Color.white;
                    }

                    // Asignar el objeto seleccionado
                    personajeSeleccionado = objetoTocado;

                    // Cambiar el color del material a rojo
                    Renderer rendererObjetoTocado = personajeSeleccionado.GetComponent<Renderer>();
                    rendererObjetoTocado.material.color = Color.red;

                    // Obtener el script espec�fico del personaje seleccionado
                    scriptPersonajeActual = personajeSeleccionado.GetComponent<Personaje>();
                }
            }
        }

        // Atacar con el personaje seleccionado con clic derecho
        if (Input.GetMouseButtonDown(1) && !ataqueEnProceso)
        {
            if (personajeSeleccionado != null)
            {
                // Obtener el script espec�fico del personaje seleccionado
                scriptPersonajeActual = personajeSeleccionado.GetComponent<Personaje>();

                // Llamar al m�todo de ataque del personaje seleccionado
                scriptPersonajeActual.Atacar();

                // Indicar que el ataque est� en proceso
                ataqueEnProceso = true;

                // Esperar un tiempo y desactivar la indicaci�n de ataque en proceso
                StartCoroutine(DesactivarIndicacionAtaqueEnProceso());
            }
        }
    } 

    IEnumerator DesactivarIndicacionAtaqueEnProceso()
    {
        yield return new WaitForSeconds(1f);
        ataqueEnProceso = false;
    }
}



