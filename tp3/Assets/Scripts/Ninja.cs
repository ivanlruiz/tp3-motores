using UnityEngine;

public class Ninjaa : MonoBehaviour
{
    public GameObject[] proyectiles; // Arreglo de proyectiles que se lanzar�n
    public float fuerzaLanzamiento = 10f; // Fuerza con la que se lanzar�n los proyectiles

    Animator animator;
    MovJugador movimiento;

    void Start()
    {
        animator = GetComponent<Animator>();
        movimiento = GetComponent<MovJugador>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            LanzarProyectiles();
        }
    }

    void LanzarProyectiles()
    {
        // Obtener la direcci�n hacia donde est� apuntando el jugador
        Vector3 direccion = transform.forward;

        // Lanzar cada proyectil en la direcci�n calculada
        foreach (GameObject proyectil in proyectiles)
        {
            GameObject nuevoProyectil = Instantiate(proyectil, transform.position + direccion, Quaternion.identity);
            Rigidbody rbProyectil = nuevoProyectil.GetComponent<Rigidbody>();
            rbProyectil.AddForce(direccion * fuerzaLanzamiento, ForceMode.Impulse);
        }
    }
}
