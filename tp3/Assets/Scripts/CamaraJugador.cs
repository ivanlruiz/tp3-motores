using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraJugador : MonoBehaviour
{
    public Transform jugador;
    public Vector3 offset;

    void LateUpdate()
    {
        transform.position = jugador.position + offset;
    }
}
