using UnityEngine;
using UnityEngine.SceneManagement;


public class MovJugador : MonoBehaviour
{
    public float velocidad = 5f;
    private Rigidbody rb;
    
    // Variables para el movimiento hacia un objetivo
    private bool movimientoHaciaObjetivo = false;
    private Vector3 objetivo;

    Animator animator;


    public float ataque = 10f; // La cantidad de da�o que el jugador inflige al enemigo

    public void Atacar(GameObject enemigo)
    {
        if (enemigo != null)
        {
            enemigo.GetComponent<Enemigo>().RecibirAtaque(ataque);
        }
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (vertical < 0f)
        {
            animator.SetBool("isRunning", true);
        }
        else animator.SetBool("isRunning", false);


        if(vertical > 0f)
        {
            animator.SetBool("isRunningBW", true);
        }
        else animator.SetBool("isRunningBW", false);

        Vector3 movimiento = new Vector3(horizontal, 0f, vertical);

        rb.velocity = movimiento * velocidad;

        if (movimientoHaciaObjetivo)
        {
            // Mover hacia el objetivo
            Vector3 direccion = (objetivo - transform.position).normalized;
            rb.MovePosition(transform.position + direccion * velocidad * Time.deltaTime);

            // Detener movimiento si se alcanza el objetivo
            if (Vector3.Distance(transform.position, objetivo) < 0.1f)
            {
                DetenerMovimiento();
            }
        }
    }
    
    public void Apuntar(Vector3 posicionObjetivo)
    {
        transform.LookAt(posicionObjetivo);
    }

    public void DesmarcarObjetivo()
    {
        objetivo = transform.position;
    }
    public void MoverA(Vector3 posicionObjetivo)
    {
        movimientoHaciaObjetivo = true;
        objetivo = posicionObjetivo;
    }

    public void DetenerMovimiento()
    {
        movimientoHaciaObjetivo = false;
        rb.velocity = Vector3.zero;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("meta"))
        {
            SceneManager.LoadScene("ganastes");
        }
    }
}