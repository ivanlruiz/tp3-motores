using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public float vida = 100f;

    private void OnMouseDown()
    {
        SeleccionarPersonaje seleccionador = FindObjectOfType<SeleccionarPersonaje>();
        if (seleccionador.personajeSeleccionado != null)
        {
            // El personaje seleccionado ataca al enemigo
            MovJugador personaje = seleccionador.personajeSeleccionado.GetComponent<MovJugador>();
            personaje.Atacar(gameObject);

            
        }
    }

    public void RecibirAtaque(float cantidad)
    {
        vida -= cantidad;
        if (vida <= 0f)
        {
            Morir();
        }
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Shuriken"))
        {
            Destroy(other.gameObject);
            vida-=10;
        }
        if (other.gameObject.CompareTag("bolaFuego"))
        {
            Destroy(other.gameObject);
            vida -= 30;
        }
    }
    private void Morir()
    {
        // Destruir el objeto enemigo
        Destroy(gameObject);
    }
    private void Update()
    {
        if (vida <= 0f)
        {
            Morir();
        }
    }
}
