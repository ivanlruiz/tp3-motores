using System.Collections.Generic;
using UnityEngine;

public class GeneradorLaberinto : MonoBehaviour
{
    public GameObject paredPrefab;
    public float anchoPared = 1.0f;
    public int anchoLaberinto = 10;
    public int altoLaberinto = 10;

    private List<Vector2Int> celdasActivas = new List<Vector2Int>();
    private Vector2Int[,] celdas;

    private void Awake()
    {
        GenerarLaberinto();
    }

    private void GenerarLaberinto()
    {
        celdas = new Vector2Int[anchoLaberinto, altoLaberinto];

        for (int i = 0; i < anchoLaberinto; i++)
        {
            for (int j = 0; j < altoLaberinto; j++)
            {
                celdas[i, j] = new Vector2Int(i, j);
            }
        }

        Vector2Int celdaInicial = new Vector2Int(Random.Range(0, anchoLaberinto), Random.Range(0, altoLaberinto));
        celdasActivas.Add(celdaInicial);

        while (celdasActivas.Count > 0)
        {
            int indiceCeldaActual = Random.Range(0, celdasActivas.Count);
            Vector2Int celdaActual = celdasActivas[indiceCeldaActual];
            celdasActivas.RemoveAt(indiceCeldaActual);

            List<Vector2Int> celdasVecinas = ObtenerCeldasVecinas(celdaActual);

            if (celdasVecinas.Count == 0)
            {
                continue;
            }

            int indiceCeldaVecina = Random.Range(0, celdasVecinas.Count);
            Vector2Int celdaVecina = celdasVecinas[indiceCeldaVecina];

            CrearParedEntreCeldas(celdaActual, celdaVecina);

            celdasActivas.Add(celdaActual);
            celdasActivas.Add(celdaVecina);
        }
    }

    private List<Vector2Int> ObtenerCeldasVecinas(Vector2Int celda)
    {
        List<Vector2Int> celdasVecinas = new List<Vector2Int>();

        if (celda.x > 0)
        {
            celdasVecinas.Add(celdas[celda.x - 1, celda.y]);
        }

        if (celda.x < anchoLaberinto - 1)
        {
            celdasVecinas.Add(celdas[celda.x + 1, celda.y]);
        }

        if (celda.y > 0)
        {
            celdasVecinas.Add(celdas[celda.x, celda.y - 1]);
        }

        if (celda.y < altoLaberinto - 1)
        {
            celdasVecinas.Add(celdas[celda.x, celda.y + 1]);
        }

        celdasVecinas.RemoveAll(x => !celdasActivas.Contains(x));

        return celdasVecinas;
    }

    private void CrearParedEntreCeldas(Vector2Int celda1, Vector2Int celda2)
    {
        Vector2Int diferencia = celda2 - celda1;

        if (diferencia.x == 1)
        {
            CrearPared(celda1, Vector3.right);
        }
        else if (diferencia.x == -1)
        {
            CrearPared(celda1, Vector3.left);
        }
        else if (diferencia.y == 1)
        {
            CrearPared(celda1, Vector3.forward);
        }
        else if (diferencia.y == -1)
        {
            CrearPared(celda1, Vector3.back);
        }
    }

    private void CrearPared(Vector2Int celda, Vector3 direccion)
    {
        Vector3 posicionPared = new Vector3(celda.x * anchoPared, 0.5f * anchoPared, celda.y * anchoPared) + direccion * 0.5f * anchoPared;
        Instantiate(paredPrefab, posicionPared, Quaternion.identity, transform);
    }
}

